const INITIAL_STATE = {
    user: '',
    data: {}
}
export default (state = INITIAL_STATE, action)=>{
    switch(action.type){
        case 'ADD_USER':
            return action.user
        case 'EDIT_USER':
            return{
                ...state,
                username: action.username,
                password: action.password
            }
        case 'ADD_DATA':
            return{
                ...state,
                data: action.data
            }
        case 'ADD_BOOTH_ID':
            return{
                ...state,
                boothId: action.id
            }
        case  'DELETE_USER':
            return {...INITIAL_STATE} 
        default:
            return state
    }
}