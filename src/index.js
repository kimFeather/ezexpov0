import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router';
import Routes from './routes';
import './index.css';
import { createStore , applyMiddleware  } from 'redux';
import { Provider } from 'react-redux';
import Reducer from './reducers'
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react'
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
// import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['email', 'username', 'password', 'data', 'boothId']
}

const persistedReducer = persistReducer(persistConfig, Reducer)

const store = createStore(persistedReducer,applyMiddleware(thunk,createLogger()))

const persistor = persistStore(store)

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Routes />
    </PersistGate>

  </Provider>


  ,
  document.getElementById('root')
);
