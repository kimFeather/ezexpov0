import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import boothPage from './components/App/boothPage';
import chartPage from './components/App/chartPage';
import lineChartPage from './components/App/lineChartPage';
import ExpoSumPage from './components/App/expoSumPage';
import ExpoLineChart from './components/App/expoLineChart';
import Login from './components/App/loginPage'
import editProfile from './components/App/editProfile'
import About from './components/About';
import NotFound from './components/NotFound';
import Profile from './components/App/Profilepage'
import ForgetPassword from './components/App/forgetPassword'
import ChangePassword from './components/App/changePassword'
import userList from './components/App/userListPage'
import createUser from './components/App/createUser'
import BoothOwnChart from './components/App/boothOwnerChartPage'
import BoothOwnLineChart from './components/App/boothOwnerLineChart'
import createSurvey from './components/App/CreateSurveyPage'
import answerSurvey from './components/App/AnswerSurvey'
import surveyList from './components/App/SurveyList'
import surveySummary from './components/App/surveySummary'
import SecurityQuestion from './components/App/SecurityQuestionPage'
import EventRecoveryPassword from './components/App/eventRecoveryPassword'
import VisitorRegistration from './components/App/VisitorRegistration'
import Announcement from './components/App/AnnoucementPage'
import VisitorRegisterForm from './components/App/VisitorRegisterForm'
import Home from './components/App/homePage'
import { connect } from "react-redux";
import _ from 'lodash'
class Routes extends Component {
componentDidMount(){
  console.log(this.props.data)
}
  render (){
    return(
    <Router >
    <Switch>
    
       <Route exact path="/" component={Home} /> 
       <Route path="/login" component={Login} /> 
      <Route path="/booth" component={boothPage} />
      <Route path="/chart" component={chartPage} />
      <Route path="/linechart" component={lineChartPage} />
      <Route path="/exposum" component={ExpoSumPage} />
      <Route path="/expolinechart" component={ExpoLineChart} />
      <Route path="/about" component={About} />
      <Route path="/profile" component={Profile} />
      <Route path="/editProfile" component={editProfile} />
      <Route path="/forgetPassword" component={ForgetPassword}/>
      <Route path="/changePassword" component={ChangePassword}/>
      <Route path="/userList" component={userList}/>
      <Route path="/createUser" component={createUser}/>
      <Route path="/boothOwnChart" component={BoothOwnChart}/>
      <Route path="/createSurvey" component={createSurvey}/>
      <Route path="/answerSurvey" component={answerSurvey}/>
      <Route path="/surveyList" component={surveyList}/>
      <Route path="/surveySummary" component={surveySummary}/>
      <Route path="/SecurityQuestion" component={SecurityQuestion}/>
      <Route path="/EventRecoveryPassword" component={EventRecoveryPassword}/>
      <Route path="/BoothOwnLineChart" component={BoothOwnLineChart}/>
      <Route path="/VisitorRegistration" component={VisitorRegistration}/>
      <Route path="/Announcement" component={Announcement}/>
      <Route path="/VisitorRegisterForm" component={VisitorRegisterForm}/>
      <Route path="*" component={NotFound} />
    </Switch>
  </Router>
    )
  }

}

 

const mapStateToProps = (state) => ({
  data: state.data
})

export default connect(mapStateToProps,null)(Routes);