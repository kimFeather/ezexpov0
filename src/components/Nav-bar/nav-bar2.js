import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import {Navbar,Nav,Form,Button,FormControl} from 'react-bootstrap'
import { connect } from 'react-redux'
class Header2 extends Component {
  logout(){
    this.props.delete()
  }
    render() {
        return (
            <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="booth">EzExpo</Navbar.Brand>
            <Nav className="mr-auto">
            {this.props.data.userRole ==="EVENTORGANIZER"? <Nav.Link href="booth">Home</Nav.Link>:<Nav.Link href="boothOwnChart">Home</Nav.Link>}
             {this.props.data.userRole ==="EVENTORGANIZER"? <Nav.Link href="userList">User management</Nav.Link>:null}
             {this.props.data.userRole ==="EVENTORGANIZER"? <Nav.Link href="surveyList">Survey</Nav.Link>:null}
             {this.props.data.userRole ==="EVENTORGANIZER"?  <Nav.Link href="VisitorRegistration">Enrollment sheet</Nav.Link>:null}
             {this.props.data.userRole ==="EVENTORGANIZER"?  <Nav.Link href="Announcement">Exhibition info</Nav.Link>:null}
              <Nav.Link href="profile">Profile</Nav.Link>
               <Nav.Link href="/login"onClick={this.logout.bind(this)}>Logout</Nav.Link>
            </Nav>
          </Navbar>
          // {this.props.iii ? : null }
        )
    }
}
const mapStateToProps = (state) => ({
  data: state.data
})
const mapDispatchToProps = (dispatch) => {
  return {
      delete: () => {
          dispatch({
              type: 'DELETE_USER',
             
          })
      }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header2) 