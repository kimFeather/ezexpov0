import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import {Navbar,Nav,Form,Button,FormControl} from 'react-bootstrap'
import { connect } from 'react-redux'
class Header3 extends Component {
  logout(){
    this.props.delete()
  }
    render() {
        return (
            <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="boothOwnChart">EzExpo</Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link href="boothOwnChart">Home</Nav.Link>
              <Nav.Link href="profile">Profile</Nav.Link>
              <Nav.Link href="/login"onClick={this.logout.bind(this)}>Logout</Nav.Link> 
            </Nav>
          </Navbar>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
  return {
      delete: () => {
          dispatch({
              type: 'DELETE_USER',
             
          })
      }
  }
}
export default connect(null, mapDispatchToProps)(Header3) 