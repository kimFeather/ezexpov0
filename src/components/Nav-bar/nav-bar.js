import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import {Navbar,Nav,Form,Button,FormControl} from 'react-bootstrap'
import { connect } from 'react-redux'


class Header extends Component {
  
  logout(){
    this.props.delete()
  }

    render() {
        return (
            <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="booth">EzExpo</Navbar.Brand>
            <Nav className="mr-auto">
              <Nav.Link href="booth">Home</Nav.Link>
              <Nav.Link href="userList">User management</Nav.Link>
              <Nav.Link href="surveyList">Survey</Nav.Link>
              <Nav.Link href="VisitorRegistration">Enrollment sheet</Nav.Link>
              <Nav.Link href="Announcement">Exhibition info</Nav.Link>
              <Nav.Link href="profile">Profile</Nav.Link>
              <Nav.Link href="/login" onClick={this.logout.bind(this)}>Logout</Nav.Link> 
            </Nav>
            <Form inline onSubmit={this.props.filter}>
              <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={this.props.handleChange} />
              <Button type="submit" variant="outline-primary">Search</Button>
            </Form>
          </Navbar>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
      delete: () => {
          dispatch({
              type: 'DELETE_USER',
             
          })
      }
  }
}

export default connect(null, mapDispatchToProps) (Header)