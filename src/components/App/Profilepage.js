import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { Card, Button, Jumbotron, Container, Form } from 'react-bootstrap';
import { connect } from 'react-redux'
import { history } from '../../system/History'
import axios from "axios"
import Header2 from '../Nav-bar/nav-bar2'
class profile extends Component {

    componentDidMount = () => {
        console.log(this.props)
    }

    goEditProfile = () => {
        history.push('/editProfile');
        history.go()
    }



    render() {
        return (
            <div>
                <Header2 />



                {/* <Card >
                        <Card.Body>
                            <Card.Title>Your Profile</Card.Title>
                            <Card.Subtitle>Username:</Card.Subtitle>
                            <Card.Text>{this.props.data.username}</Card.Text>
                            <Card.Subtitle>Email:</Card.Subtitle>
                            <Card.Text>{this.props.data.email}</Card.Text>
                            <Card.Subtitle>Name:</Card.Subtitle>
                            <Card.Text>{this.props.data.firstName} {this.props.data.lastName}</Card.Text>
                            <Button variant="primary"  onClick={() => this.goEditProfile()}>Edit Profile</Button>
                        </Card.Body>
                    </Card> */}

                <Jumbotron fluid>
                    <Container>
                        <h1 style={{ fontSize: '30px', fontWeight: 'bold', marginBottom: '5px' }}>Your profile</h1>


                        <h3 style={{ fontSize: '20px', marginBottom: '5px' }}>Username</h3>
                        <Form.Control  type="text" defaultValue={this.props.data.username} disabled/>
                        <h3 style={{ fontSize: '20px', marginBottom: '5px' }}>Email</h3>
                        <Form.Control  type="text" defaultValue={this.props.data.email} disabled/>
                        <h3 style={{ fontSize: '20px', marginBottom: '5px' }}>Name</h3>
                        <Form.Control  type="text" defaultValue={[this.props.data.firstName +' '+ this.props.data.lastName]} disabled/>
                        <h3 style={{ fontSize: '20px', marginBottom: '5px' }}>Phone number</h3>
                        <Form.Control  type="text" defaultValue={this.props.data.phoneNumber} disabled/>
                        <p style={{marginTop:'15px', marginLeft:'90%'}}>
                            <Button variant="primary" onClick={() => this.goEditProfile()}>Edit Profile</Button>
                        </p>
                    </Container>

                </Jumbotron>

            </div>

        )
    }
}
const mapStateToProps = (state) => ({
    data: state.data,
    password: state.password
})
export default connect(mapStateToProps, null)(profile)