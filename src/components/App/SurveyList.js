import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { Button, Card, Row, Col } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import axios from "axios";
import _ from 'lodash';
import Header2 from '../Nav-bar/nav-bar2'
import { connect } from 'react-redux';
import { dispatch } from 'rxjs/internal/observable/pairs';

class surveyList extends Component {
    state = {
        surveyList: []
    }
    createSurvey = () => {
        history.push('/createSurvey');
        history.go()
    }
    answerSurvey = (id) => {
        this.props.addSurveyId(id)
        history.push('/answerSurvey');
        history.go()
    }
    surveySummary = (id) => {
        this.props.addSurveyId(id)
        history.push('/surveySummary');
        history.go()
    }
    componentDidMount = () => {
        try {
            axios({
                url: `http://localhost:8080/survey`,
                method: 'get'
            }).then(res => {
                this.setState({
                    surveyList: res.data
                })
                console.log('surveylist', this.state.surveyList)
                console.log('surveylist', this.state.surveyList.surveyName)
            }).catch(error => {
                console.error(error);
                alert('Cannot retrieve data')
            });
        } catch (error) {
            console.error(error);
            alert('Cannot retrieve data')
        }
    }
    render() {
        return (
            <div>
                <Header2 />
                <div >
                    <Row style={{ marginTop: '2%', marginLeft: '2%', marginBottom: '1%' }}>
                        <Col>
                            <h2 style={{ fontSize: "40px", fontWeight: "bold" }}>All surveys</h2>

                        </Col>
                        <Col xs lg="2" ><Button variant="primary" type="submit" onClick={this.createSurvey.bind(this)} >
                            Create new survey
                         </Button>
                        </Col>
                    </Row>

                    <div>
                        {/* {this.state.surveyList.map(surveyList => (
                            <Card style={{ width: '20em', marginLeft: '1em', height: '10em' }}>
                                <Card.Body>
                                    <Card.Title>{surveyList.surveyName}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{surveyList.surveyDescription}</Card.Subtitle>

                                    <div className="survayListButt">
                                        <Button style={{ marginRight: '10px' }} variant="success" onClick={() => this.answerSurvey(surveyList.id)}>Answer survey</Button>
                                        <Button variant="info" onClick={() => this.surveySummary(surveyList.id)}>Summary</Button>
                                    </div>

                                </Card.Body>
                            </Card>
                        ))} */}
                        {_.map(_.chunk(_.values(this.state.surveyList), 4), (value, key) => {
                            return (<Row className="justify-content-md-center d-inline-flex" key={key} style={{ margin: '20px', paddingBottom: '10px' }}>
                                {_.map(value, (val, k) => {
                                    console.log(val);
                                    return (<Col key={k}><Card style={{ width: '20em', height: '10em' }}>
                                        <Card.Body>
                                            <Card.Title>{val.surveyName}</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">{val.surveyDescription}</Card.Subtitle>
                                            <div className="survayListButt">
                                                <Button style={{ marginRight: '10px' }} variant="success" onClick={() => this.answerSurvey(val.id)}>Answer survey</Button>
                                                <Button variant="info" onClick={() => this.surveySummary(val.id)}>Summary</Button>
                                            </div>
                                        </Card.Body>
                                    </Card></Col>)
                                })}
                            </Row>)
                        })}
                        
                    </div>



                </div>

            </div >
        )
    }
}
const mapDidpatchToProps = (dispatch) => {
    return {
        addSurveyId: (id) => {
            dispatch({ type: 'ADD_BOOTH_ID', id: id })
        }
    }
}
export default connect(null, mapDidpatchToProps)(surveyList)