import React, { Component } from 'react';
import axios from "axios";
import { Form, Card, Button, Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import 'bootstrap/dist/css/bootstrap.min.css';

export default class RegisterForm extends Component {
    state = {
        name: '',
        email: '',
        phoneNumber: '',
        announcement:[]
    }
    _handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
        console.log(this.state.name)
        console.log(this.state.email)
        console.log(this.state.phoneNumber)
    }

    componentDidMount =()=>{
        try {
            axios({
                url: `http://localhost:8080/announcement/id?id=88`,
                method: 'get'
            }).then(res => {
                this.setState({
                    announcement: res.data
                })
                console.log('announcement', this.state.announcement)
            
            }).catch(err => {
                console.warn(err)
                alert('Cannot retreive data')
                return;
            });
        } catch (error) {
            console.error(error)
        }
    }

    createUser = (event) => {
        event.preventDefault();
            try {
                axios({
                    url: `http://localhost:8080/addVisitor`,
                    method: 'post',
                    data: {
                        name: this.state.name,
                        email: this.state.email,
                        phoneNumber:this.state.phoneNumber
                    }
                }).then(() => {
                    alert('Register complete')
                    window.location.reload();
                }).catch(err => {
                    console.warn(err)
                    alert('Cannot register')
                    return;
                });
            } catch (error) {
                console.error(error)
            }
        }
      

 
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>
                <div style={{width:'40%',margin:'auto', marginTop:'10em'}}>
                    <Container>
                        <Form style={{ marginTop: '2%', marginLeft: '2%', marginBottom: '1%' }} onSubmit={this.createUser.bind(this)} >
                            <p style={{ fontSize: "40px", fontWeight: 'bold' }}>Pre-registor the exhibition</p>
                            <p style={{ fontSize: "20px", fontWeight: 'bold', color:'red' }}>**{this.state.announcement.announcement}</p>
                            <Form.Group >
                                <Form.Label>Name : </Form.Label>
                                <Form.Control name="name" type="text" onChange={this._handleChange.bind(this)} required />
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Email address: </Form.Label>
                                <Form.Control name="email" type="email" onChange={this._handleChange.bind(this)} required />
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Phone number: </Form.Label>
                                <Form.Control name="phoneNumber" type="text" onChange={this._handleChange.bind(this)} required />
                            </Form.Group>
                            <div style={{marginLeft:'42%'}}>
                                 <Button variant="primary" type="submit" >Register</Button>
                            </div>
                        </Form>
                    </Container>
                </div>


            </div>
        )
    }
}