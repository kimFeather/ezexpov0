import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import 'typeface-roboto';
import axios from "axios"
import { Form, Card, Button, Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'

class ForgetPassword extends Component {
    state = {
        email: ''
    }
    loginWithEmail = (event) => {
        event.preventDefault();
        axios({
            url: `http://localhost:8080/forgotPassword?email=${this.state.email}`,
            method: 'get',

        }).then(res => {
            const { data } = res
            const { user } = data
            console.log(data)
            if (data === "not found") {
                alert('No account found')
            } else {
                this.props.adddata(data)
                console.log(this.props)
                history.push('/SecurityQuestion');
                history.go()
            }


        }).catch(e => {
            console.log("error " + e)
            alert("Cannot retrive data")
        })
    }
    onEmailChange = event => {
        const email = event.target.value;
        this.setState({ email });
        console.log(this.state.email)
    };
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>
                <Container>
                    <Form style={{ margin: '10%' }} onSubmit={this.loginWithEmail.bind(this)}>
                        <Form.Label style={{ fontSize: '30px', textalign: 'center' }}>Enter your email</Form.Label>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control onChange={this.onEmailChange} type="email" placeholder="Enter email" required />

                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                  </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}

const mapDidpatchToProps = (dispatch) => {
    return {
        adddata: (data) => {
            dispatch({
                type: 'ADD_DATA',
                data: data
            })
        }

    }
}

export default connect(null, mapDidpatchToProps)(ForgetPassword)