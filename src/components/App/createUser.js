import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import { Form, Card, Button } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import axios from "axios";
import _ from 'lodash';
import Header2 from '../Nav-bar/nav-bar2'


export default class createUser extends Component {
    state = {
        firstname: '',
        lastname: '',
        email: '',
        username: '',
        password: '',
        phoneNumber:'',
        confirmPassword: '',
        securityQuestion:'',
        securityAnswer:'',
        boothName:'',
        userRole: 'BOOTHOWNER'

    }

    _handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })

    }
    onFirstNameChange = event => {
        const firstname = event.target.value;
        this.setState({ firstname });
        console.log(this.state.firstname)
    };
    onLastNameChange = event => {
        const lastname = event.target.value;
        this.setState({ lastname });
        console.log(this.state.lastname)
    };
    onConfirmPasswordChange = event => {
        const confirmPassword = event.target.value;
        this.setState({ confirmPassword });
        console.log(this.state.confirmPassword)
    }
   
    changeQuestion = event =>{
        this.setState({securityQuestion: event.target.value});
        console.log(this.state.securityQuestion)

    }




    createUser = (event) => {
        console.log('name', this.state.firstname)
        console.log('lastname', this.state.lastname)
        console.log('username', this.state.username)
        console.log('password', this.state.password)
        console.log('confirmPass', this.state.confirmPassword)
        console.log('Question',this.state.securityQuestion)
        event.preventDefault();
        if (_.size(this.state.password) < 6) {
            alert('The password is to short');
            return;
        }
        if (this.state.password == this.state.confirmPassword) {
            try {
                axios({
                    url: `http://localhost:8080/newBoothOwnerTest`,
                    method: 'post',
                    data: {
                        firstName: this.state.firstname,
                        lastName: this.state.lastname,
                        email: this.state.email,
                        username: this.state.username,
                        password: this.state.password,
                        phoneNumber:this.state.phoneNumber,
                        boothName: this.state.boothName,
                        securityQuestion:this.state.securityQuestion,
                        securityAnswer:this.state.securityAnswer,
                        userRole: this.state.userRole

                    }
                }).then(() => {
                    alert('Create new boothowner success')
                    history.push('/userList')
                    history.go()
                }).catch(err => {
                    console.warn(err)
                    alert('Cannot create boothowner account')
                    return;
                });
            } catch (error) {
                console.error(error)
            }
        }
        if (this.state.password != this.state.confirmPassword) {
            alert('Password does not match')
            return;
        }

    }
    render() {

        return (
            <div>
                <Header2 />
                <Container>

                    <Form style={{ marginTop: '2%', marginLeft: '2%', marginBottom: '1%' }} onSubmit={this.createUser.bind(this)} >
                        <p style={{ fontSize: "40px", fontWeight: 'bold' }}>New boothowner</p>
                        <Form.Group >
                            <Form.Label>First name: </Form.Label>
                            <Form.Control name="firstName" type="text" onChange={this.onFirstNameChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Last name: </Form.Label>
                            <Form.Control name="lastName" type="text" onChange={this.onLastNameChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Username: </Form.Label>
                            <Form.Control name="username" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Email address: </Form.Label>
                            <Form.Control name="email" type="email" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Phone number: </Form.Label>
                            <Form.Control name="phoneNumber" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Booth name: </Form.Label>
                            <Form.Control name="boothName" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Password: </Form.Label>
                            <Form.Control name="password" type="password" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Confirm password: </Form.Label>
                            <Form.Control name="password" type="password" onChange={this.onConfirmPasswordChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Select your security question</Form.Label>
                            <Form.Control as="select" onChange={this.changeQuestion.bind(this)}>
                                <option value= "What is the name of your favorite childhood friend?" >What is the name of your favorite childhood friend?</option>
                                <option value="What school did you attend for sixth grade?">What school did you attend for sixth grade?</option>
                                <option value="What was your childhood nickname?">What was your childhood nickname?</option>
                                <option value="What is the country of your ultimate dream vacation?">What is the country of your ultimate dream vacation?</option>
                                <option value="Who was your childhood hero?">Who was your childhood hero?</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Your security answer:</Form.Label>
                            <Form.Control name="securityAnswer" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Button variant="primary" type="submit" >Create new boothowner</Button>
                    </Form>
                </Container>

            </div>

        )
    }
}
