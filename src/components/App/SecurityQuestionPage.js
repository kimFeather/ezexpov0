import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import 'typeface-roboto';
import axios from "axios"
import { Form, Card, Button, Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'

class SecurityQuestion extends Component {
    state = {
        securityAnswer: ''
    }

    onSecurityAnswerChange = event => {
        const securityAnswer = event.target.value;
        this.setState({ securityAnswer });
        console.log(this.state.securityAnswer)
    };
    submitAnswer = (event) => {
        console.log(this.state.securityAnswer)
        event.preventDefault();
        if (this.state.securityAnswer == this.props.data.securityAnswer) {
            history.push('/changePassword')
            history.go()
        } else {
            alert('Incorrect answer, if you cannot remember the answer please contact event organizer directly.')
        }
    }
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>
                <Container>
                    <Form style={{ margin: '10%' }} onSubmit={this.submitAnswer.bind(this)} >
                        <h3 style={{ fontSize: "20px", fontWeight: 'bold' }}>{this.props.data.securityQuestion}</h3>
                        <Form.Group>
                            <Form.Label>Your Answer</Form.Label>
                            <Form.Control onChange={this.onSecurityAnswerChange} type="text" placeholder="Answer" required />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                  </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.data
})
export default connect(mapStateToProps, null)(SecurityQuestion)