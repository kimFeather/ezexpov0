import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import { Router, Route } from 'react-router';
import { id } from 'postcss-selector-parser';
// import { connect } from "react-redux";
import { Button, Card } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import SearchField from "react-search-field"
import axios from "axios"
import { history } from '../../system/History'
import { connect } from 'react-redux'
import Header from '../Nav-bar/nav-bar'
import BarChart from '../Chart/barChart';
import PieChart from '../Chart/pieChart';
import _ from 'lodash';
import { FiUser } from 'react-icons/fi';


class BoothPage extends Component {

    state = {
        boothList: [],
        search: ''
    };

    handleChange = (event) => {
        const value = event.target.value
        this.setState({ search: value })
    }

    Onsearch = (event) => {
        event.preventDefault()
        const booth = this.state.boothList.filter(booth => {
            return booth.boothName.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase())
        })
        console.log(booth)
        if (booth.length) {
            this.setState({ boothList: booth })
        } else {
            alert("No exhibition found")
            this.componentWillMount()
        }
    }


    onClickBooth = (id) => {
        this.props.addBoothId(id)
        history.push("/chart");
        history.go()

    }

    onClickSum = () => {
        history.push('/exposum');
        history.go()

    }

    componentDidMount = () => {
        console.log("componentWillMount");
        axios
            .get(`http://localhost:8080/booth`)
            .then(res => {
                console.log(res);
                console.log("in axios");
                this.setState({ boothList: res.data });
            })
            .catch(error => {
                console.log(error)
                alert('Cannot retrieve data')
            })
    }
    render() {
        return (
            <div className="Background">
                <Header filter={this.Onsearch.bind(this)} handleChange={this.handleChange.bind(this)} />
                <div class="header" className="ExpoHeader">

                    <div class="backButton" style={{ paddingTop: "1%", paddingRight: "2%" }} >
                    
                    </div>

                    <div class="expoName" className="ExpoHeaderFont">
                       SE Show pro 2019
                    </div>

                    <div style={{marginTop:'1%',marginLeft:'3%'}}>
                        <Button variant="info"  onClick={this.onClickSum} >EXPO Summary</Button>
                    </div>


                </div>
                <div class="flex-container" >

                    <div class="content" className="RowContent" >

                        {this.state.boothList.map(booth => (
                            <div className="Block" onClick={() => this.onClickBooth(booth.id)}>
                                <div className="userInBlock">
                                    <FiUser size={20} color={'red'} />
                                    {_.sum(_.values(_.mapValues(booth.visitor, 'total')))}
                                </div>
                                <div className="boothNameInBlock">{booth.boothName}</div>
                            </div>
                        ))}

                    </div>
                </div>
                {/* footer */}
                <div>

                </div>
            

            </div>
        )
    }
}
const mapDispa = (dispatch) => {
    return {
        addBoothId: (id) => {
            dispatch({ type: 'ADD_BOOTH_ID', id: id })
        }
    }
}
export default connect(null, mapDispa)(BoothPage)
