import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import Chart from './Chart';
import BarChart from '../Chart/barChart';
import DoughnutChart from '../Chart/doughnutChart';
import PieChart from '../Chart/pieChart';
import LineChart from '../Chart/lineChart';
import { icon } from 'react-bulma-components';
import { black } from 'ansi-colors';
import axios from "axios"
import {history} from '../../system/History'
import { connect } from "react-redux";
import Header3 from '../Nav-bar/nav-barBooth'
import _ from 'lodash'
// import { connect } from "react-redux";
//import 

class BoothOwnerLineChartPage extends Component {

  constructor() {
    super();
    this.state = {
      chartData: {},
      visitorData:{},
      boothList: [],
    }
  }

  componentDidMount() {
    console.log(this.props.data,'ss')

    axios
      .get(`http://localhost:8080/booth/userId?id=${this.props.data.id}`)
      .then(res => {
      this.setState({boothList: res.data})
      this.getChartData(res);
      }).catch(error => {
        console.log(error)
        alert('Cannot retrieve data')
    })



  }

  onClickBack = () => {

    history.push('/chart');
    history.go()
  }

  


  getChartData(res) {
    // Ajax calls here
  
    const xLabel =_.mapValues(this.state.boothList.visitor, 'visitTime')
    console.log(xLabel)
    let test = []
    _.forEach(xLabel, val => {
        test = _.unionWith(test, [val], _.isEqual)
    })
    console.log(test);
    const cloneTest = _.cloneDeep(test)
        this.setState({
          boothList: res.data,
          chartData: {
            labels:  _.map(cloneTest,o=>_.join(_.pullAt(o,[0,1,2]),'-')+' '+_.join(o,':')+'0'),
            datasets: [
              {
                label: 'Happy',
                data: _.values(_.mapValues(res.data.visitor, 'happy')),
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)'
                ],
                borderColor:'rgba(255, 99, 132, 0.6)',
                fill: false
              },
              {
                label: 'Sad',
                data: _.values(_.mapValues(res.data.visitor, 'sad')),
                backgroundColor: [
                  'rgba(54, 162, 235, 0.6)'
                ],
                borderColor:'rgba(54, 162, 235, 0.6)',
                fill: false
              },
              {
                label: 'Surprise',
                data:  _.values(_.mapValues(res.data.visitor, 'surprise')),
                backgroundColor: [
                  'rgba(255, 206, 86, 0.6)'
                ],
                borderColor: 'rgba(255, 206, 86, 0.6)',
                fill: false
              },
              {
                label: 'Angry',
                data:  _.values(_.mapValues(res.data.visitor, 'angry')),
                backgroundColor: [
                  'rgba(75, 192, 192, 0.6)'
                ],
                borderColor:'rgba(75, 192, 192, 0.6)',
                fill: false
              },
              {
                label: 'Distgust',
                data: _.values(_.mapValues(res.data.visitor, 'disgust')),
                backgroundColor: [
                  'rgba(153, 102, 255, 0.6)'
                ],
                borderColor:'rgba(153, 102, 255, 0.6)',
                fill: false
              },
              {
                label: 'Fear',
                data: _.values(_.mapValues(res.data.visitor, 'fear')),
                backgroundColor: [
                  'rgba(255, 159, 64, 0.6)'
                ],
                borderColor:'rgba(255, 159, 64, 0.6)',
                fill: false
              },
              {
                label: 'Netural',
                data: _.values(_.mapValues(res.data.visitor, 'netural')),
                backgroundColor: [
                  'rgba(255, 100, 132, 0.6)'
                ],
                borderColor: 'rgba(255, 100, 132, 0.6)',
                fill: false
              }
              

            ]
          }
        });
    console.log(this.state.boothList, 'hello')

  }

  render() {
    console.log(this.state.visitorData)
    return (
      <div className="Background" >
        {/* hearder */}
        <Header3/>
        < div class="header" className="ExpoHeader" >

          {/* <div class="backButton" style={{ paddingTop: "1%", paddingRight: "2%" }} >
            <img width="40" height="40" src={require('../../assets/back.png')} onClick={this.onClickBack} />
          </div> */}
          <div class="expoName" className="ExpoHeaderFont">

            <p> {this.state.boothList.firstName} Booth Infomation</p>

          </div>
        </div>



        {Object.keys(this.state.chartData).length && <div className="linechartBlock" >
          <LineChart chartData={this.state.chartData} location="bar" legendPosition="bottom" />
          
        </div>
        }

      </div >
    );
  }

}

const mapStateToProps =(state)=>{
  return {
    data: state.data
  }
}

export default connect(mapStateToProps, null)(BoothOwnerLineChartPage);