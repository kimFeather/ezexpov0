import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { Button } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import axios from "axios";
import _ from 'lodash';
import { connect } from 'react-redux';
import Header2 from '../Nav-bar/nav-bar2'
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import TableHeaderColumn from 'react-bootstrap-table-next'

class userList extends Component {
    state = {
        userList: []
    }

    componentWillMount() {
       try{
        axios({
            url: `http://localhost:8080/User?role=BOOTHOWNER`,
            method: 'get'
        }).then(res => {
            this.setState({
                userList: res.data
            })
            console.log('userlist', this.state.userList)
        })
     .catch(error =>{
        console.log(error)
        alert('Cannot retrieve data')
     }) 
     } catch (error) {
        console.error(error);
        alert('Cannot retrieve data')
    }
           
    }
    createNewUser(){
        history.push('/createUser')
        history.go()
    }
    recoveryPassword=(id)=>{
        this.props.addUserId(id)
        setTimeout(() => {
              history.push('/EventRecoveryPassword')
        history.go()
        }, 1000)
      
    }
   
    render() {
        const buttonFormatter=(cell, row)=>{
            return <div>
                <Button variant="danger" type="submit" onClick={() => this.recoveryPassword(row.id)}>Change password</Button>
            </div>
        }
        const columns = [
        {
            dataField: 'id',
            text: 'Id',
            sort: true,
            headerStyle: { textAlign: 'center'},
        },
        {
            dataField: 'username',
            text: 'Username',
            sort: true,
            headerStyle: { textAlign: 'center'},
            filter: textFilter()
        },{
            dataField: 'firstName',
            text: 'Firstname',
            headerStyle: { textAlign: 'center'},
            sort: true
            
        }, {
            dataField: 'lastName',
            text: 'Lastname',
            headerStyle: { textAlign: 'center'},
            sort: true
        }, {
            dataField: 'email',
            text: 'Email',
            headerStyle: { textAlign: 'center'},
            sort: true
        },
        {
            dataField: 'phoneNumber',
            text: 'Phone number',
            headerStyle: { textAlign: 'center'},
        },
        {
            formatter: buttonFormatter
        }
     
        ];
         
        return (
            <div>
                <Header2 />
                <div style={{marginLeft:'85%',marginTop:'1em',marginBottom:'1em'}}>
                    <Button variant="primary" type="submit" onClick={this.createNewUser.bind(this)} >
                      Create new boothowner
                    </Button>
                </div>
                <BootstrapTable keyField='username' data={this.state.userList} columns={columns} filter={filterFactory()} striped hover condensed>
                <TableHeaderColumn dataField="button" dataFormat={buttonFormatter}>Button</TableHeaderColumn>
                </BootstrapTable> 
               
            </div>

        )
    }
}

const mapDidpatchToProps = (dispatch) => {
    return {
        addUserId: (id) => {
            dispatch({ type: 'ADD_BOOTH_ID', id: id })
        }
    }
}
const mapStateToProps = (state) => {
    return {
        id: state.boothId
    }
}
export default connect(mapStateToProps, mapDidpatchToProps)(userList)