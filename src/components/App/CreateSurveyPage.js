import React, { Component } from 'react';
import { Button ,Form } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import axios from "axios";
import Header2 from '../Nav-bar/nav-bar2'
import { connect } from 'react-redux'

class createSurvey extends Component{
    state={
        data:{
            question1:'What is your level of satisfaction for this exhibition?',
            question2:'How convenience of our exhibition hours?',
            question3:'How was staff’s friendliness and courtesy?',
            question4:'How likely are you to recommend this exhibition to a friend?',
            question5:'Are you likely to participate in one of our exhibition in the future?'
        }
    }
    
    
    
    _handleChange = event => {
        this.setState({ data:{
            ...this.state.data,
            [event.target.name]: event.target.value }
        })
    }
    createSurvey = (event)=>{
        event.preventDefault();
        console.log(this.state.data)
        try {
            axios({
                url: `http://localhost:8080/createSurvey`,
                method: 'post',
                data: {
                    ...this.state.data
                },
                headers:{
                    Authorization: `Bearer ${this.props.data.token}`
                },
            }).then(() => {
                alert('Create survey success')
                history.push('/surveyList')
                history.go()
                
            }).catch(err => {
                console.warn(err)
                alert('Cannot create survey')
                return;
            });
        } catch (error) {
            console.error(error)
        }
    }
    render(){
        return(
            <div>
                <Header2/>
                <Container>
                    <Form style={{ marginLeft: '10%' , marginTop: '2%' }}  onSubmit={this.createSurvey.bind(this)} >
                        <p style={{ fontSize:'40px',  fontWeight:"bold", marginBottom:'2%' }}>Create Survey</p>
                        <Form.Group >
                            <Form.Label>Survey name: </Form.Label>
                            <Form.Control name="surveyName" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Survey description: </Form.Label>
                            <Form.Control name="surveyDescription" type="text" onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>First survey question: </Form.Label>
                            <Form.Control name="question1" type="text"  defaultValue={'What is your level of satisfaction for this exhibition?'} onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Second survey question: </Form.Label>
                            <Form.Control name="question2" type="text"  defaultValue={'How convenience of our exhibition hours?'} onChange={this._handleChange.bind(this)} required/>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Third survey question: </Form.Label>
                            <Form.Control name="question3" type="text"  defaultValue={'How was staff’s friendliness and courtesy?'} onChange={this._handleChange.bind(this)} required/>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Fourth survey question: </Form.Label>
                            <Form.Control name="question4" type="text"  defaultValue={'How likely are you to recommend this exhibition to a friend?'} onChange={this._handleChange.bind(this)} required/>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Fifth survey question: </Form.Label>
                            <Form.Control name="question5" type="text" defaultValue={'Are you likely to participate in one of our exhibition in the future?'} onChange={this._handleChange.bind(this)} required/>
                        </Form.Group>
                        <Button variant="primary" type="submit" >Create survey</Button>
                    </Form>
                </Container>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    data: state.data
})
export default connect(mapStateToProps,null)(createSurvey) 