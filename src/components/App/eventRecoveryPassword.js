import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { Button, Form, Card, Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import FormCheck from 'react-bootstrap/FormCheck'
import { history } from '../../system/History'
import axios from "axios";
import Header2 from '../Nav-bar/nav-bar2'
import { connect } from 'react-redux';
import _ from 'lodash';


class eventRecoveryPassword extends Component {
    state = {
        User: [],
        newPassword: '',
        confirmPassword: ''
    }
    onNewPasswordChange = event => {
        const newPassword = event.target.value;
        this.setState({ newPassword });
        console.log(this.state.newPassword)
    };
    onConfirmPasswordChange = event => {
        const confirmPassword = event.target.value;
        this.setState({ confirmPassword });
        console.log(this.state.confirmPassword)
    };
    changePassword = (event) => {
        event.preventDefault()
        if(_.size(this.state.newPassword) < 6){
            alert('The password is to short');
            return;
        }
        if (this.state.newPassword == this.state.confirmPassword) {
            try {
                axios({
                    url: `http://localhost:8080/updateuser?userid=${this.props.id}`,
                    method: 'put',
                    data: {
                        password: this.state.newPassword
                    }
                }).then(res => {
                    alert('Password change completed')
                    history.push('/userList');
                    history.go()
                }).catch(err => {
                    console.warn(err)
                    alert('Cannot change your password')
                    return;
                });
            } catch (e) {
                console.warn(e)
            }
        } else {
            alert('Password are not matched')
        }


    }
    componentWillMount = () => {
        try {
            axios({
                url: `http://localhost:8080/UserById?id=${this.props.id}`,
                method: 'get',

            }).then(res => {
                this.setState({
                    User: res.data
                })
                console.log('user', this.state.User)
            }).catch(e => {
                console.log("error " + e)
                alert("Cannot retreive data")
                return;
            })

        } catch (e) {
            console.warn(e)
        }

    }
    render() {
        return (
            <div>
                <Header2/>
                <Container>
                    <Form style={{ margin: '20em' }} onSubmit={this.changePassword.bind(this)} >
                        <p style={{ fontWeight: 'bold', fontSize: '20px' }}>Account: {this.state.User.username}</p>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control onChange={this.onNewPasswordChange} type="password" placeholder="New password" required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control onChange={this.onConfirmPasswordChange} type="password" placeholder="Confirm new password" required />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                  </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        id: state.boothId
    }
}
export default connect(mapStateToProps, null)(eventRecoveryPassword) 