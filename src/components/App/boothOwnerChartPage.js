import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import Chart from './Chart';
import BarChart from '../Chart/barChart';
import DoughnutChart from '../Chart/doughnutChart';
import Linechart from './linechart'
import PieChart from '../Chart/pieChart';
import LineChart from '../Chart/lineChart';
import { icon } from 'react-bulma-components';
import { black } from 'ansi-colors';
import axios from "axios"
import {history} from '../../system/History'
 import { connect } from "react-redux";
 import Header3 from '../Nav-bar/nav-barBooth'
 import { Button } from 'react-bootstrap'
 import _ from 'lodash'

class BoothOwnchart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chartData: {},
      LinechartData: {},
      visitorData: {},
      emotionData: {},
      boothList: [],
      charOverallData: {}
    }
  }

  componentDidMount() {

    // console.log(this.props)
    // console.log(this.props.location)
    axios
      .get(`http://localhost:8080/booth/userId?id=${this.props.data.id}`)
      .then(res => {
        // console.log(res);
        // console.log("in axios");
        this.getChartData(res);
        //this.getLineChartData(res);
        this.getVisitorData(res.data);
        this.getOverAllChartData(res)
        this.lodashGetBoothData(res)

      }).catch(error => {
        console.log(error)
        alert('Cannot retrieve data')
    })





  }

  onClickBack = () => {
    history.push('/booth');
    history.go()
  }

  onClickBarchart = (id) => {
    history.push({ pathname: '/BoothOwnLineChart', query: { id } });
    history.go()

  }

  lodashGetBoothData = () => {
    const test = _.sum(_.values(_.mapValues(this.state.boothList.visitor, 'total')))
    console.log(test)
  }

  getBoothData(boothList) {
    //happy
    var avgHappy = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.happy);
    }, 0) / boothList['visitor'].length;
    //console.log("happy avg = " + avgHappy)

    //sad
    var avgSad = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.sad);
    }, 0) / boothList['visitor'].length;
    //console.log("sad avg = " + avgSad)

    //surprise
    var avgSurprise = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.surprise);
    }, 0) / boothList['visitor'].length;
    // console.log("surprise avg = " + avgSurprise)

    //angry
    var avgAngry = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.angry);
    }, 0) / boothList['visitor'].length;
    //console.log("angry avg = " + avgAngry)

    //disappoint
    var avgDisappoint = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.disgust);
    }, 0) / boothList['visitor'].length;
    //console.log("disappoint avg = " + avgDisappoint)

    //fear
    var avgFear = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.fear);
    }, 0) / boothList['visitor'].length;
    //console.log("fear avg = " + avgFear)

    //netural
    var avgNetural = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.netural);
    }, 0) / boothList['visitor'].length;
    //console.log("netural avg = " + avgNetural)

    //av==emotion
    var avgEmotion = boothList['visitor'].reduce((sum, visitor) => {
      return sum
        + parseFloat(visitor.happy)
        + parseFloat(visitor.sad)
        + parseFloat(visitor.surprise)
        + parseFloat(visitor.angry)
        + parseFloat(visitor.disgust)
        + parseFloat(visitor.fear)
        + parseFloat(visitor.netural);
    }, 0) / boothList['visitor'].length;
    //console.log("emotion avg = " + avgEmotion)

    this.setState({
      emotionData: {
        avgHappy,
        avgSad,
        avgSurprise,
        avgAngry,
        avgDisappoint,
        avgFear,
        avgNetural,
        avgEmotion
      }
    })

    return [avgHappy, avgSad, avgSurprise, avgAngry, avgDisappoint, avgFear, avgNetural]

  }


  getOverallBoothData(boothList) {
    //happy
    var Happy = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.happy);
    }, 0)
    //console.log("happy = " + Happy)

    //sad
    var Sad = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.sad);
    }, 0)
    //console.log("sad  = " + Sad)

    //surprise
    var Surprise = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.surprise);
    }, 0)
    //console.log("surprise  = " + Surprise)

    //angry
    var Angry = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.angry);
    }, 0)
    //console.log("angry = " + Angry)

    //disappoint
    var Disappoint = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.disgust);
    }, 0)
    //console.log("disappoint  = " + Disappoint)

    //fear
    var Fear = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.fear);
    }, 0)
    //console.log("fear  = " + Fear)

    //netural
    var Netural = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.netural);
    }, 0)
    //console.log("netural  = " + Netural)

    return [Happy, Sad, Surprise, Angry, Disappoint, Fear, Netural]
  }

  getVisitorData(boothList) {

    //happy
    var avgHappy = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.happy);
    }, 0) / boothList['visitor'].length;

    //sad
    var avgSad = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.sad);
    }, 0) / boothList['visitor'].length;

    //surprise
    var avgSurprise = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.surprise);
    }, 0) / boothList['visitor'].length;

    //angry
    var avgAngry = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.angry);
    }, 0) / boothList['visitor'].length;

    //disappoint
    var avgDisappoint = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.disgust);
    }, 0) / boothList['visitor'].length;

    //fear
    var avgFear = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.fear);
    }, 0) / boothList['visitor'].length;

    //netural
    var avgNetural = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.netural);
    }, 0) / boothList['visitor'].length;

    //av==emotion
    var avgEmotion = boothList['visitor'].reduce((sum, visitor) => {
      return sum
        + parseFloat(visitor.happy)
        + parseFloat(visitor.sad)
        + parseFloat(visitor.surprise)
        + parseFloat(visitor.angry)
        + parseFloat(visitor.disgust)
        + parseFloat(visitor.fear)
        + parseFloat(visitor.netural);
    }, 0) / boothList['visitor'].length;

    //men visitor
    var avgMenVisitor = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.numOfMale);
    }, 0);
    //console.log("visitor men avg = " + avgMenVisitor)

    //womenmen visitor
    var avgWomenVisitor = boothList['visitor'].reduce((sum, visitor) => {
      return sum + parseFloat(visitor.numOfFemale);
    }, 0);
    //console.log("visitor men avg = " + avgWomenVisitor)

    var avgVisitor = avgMenVisitor + avgWomenVisitor
    // console.log("men + women :" + avgVisitor)

    this.setState({
      visitorData: {
        avgHappy,
        avgSad,
        avgSurprise,
        avgAngry,
        avgDisappoint,
        avgFear,
        avgNetural,
        avgEmotion,
        avgMenVisitor,
        avgWomenVisitor,
        avgVisitor
      }
    })

  }


  getTimeStampData(boothList) {
    //happy per time stamp
    var happyTimestamp = boothList['visitor'].filter()
    //console.log("visitor men avg = " + happyTimestamp)
  }


  getChartData(res) {
    // Ajax calls here
    this.setState({
      boothList: res.data,
      chartData: {
        labels: ['Happy', 'Sad', 'Suprise', 'Angry', 'Disgust', 'Fear', 'Netural'],
        datasets: [
          {
            label: 'Visitor',
            data: this.getBoothData(res.data),
            backgroundColor: [
              'rgba(255, 206, 86, 0.6)',
              'rgba(54, 162, 235, 0.6)',
              'rgba(255, 159, 64, 0.6)',
              ' rgb(255, 154, 162)',
              ' rgb(149, 125, 173)',
              'rgb(170, 169, 173)',
              'rgb(181, 234, 215)',
            ]
          }
        ]
      }
    });
    //console.log(this.state.boothList, 'hello')

  }

  getOverAllChartData(res) {
    this.setState({
      boothList: res.data,
      charOverallData: {
        labels: ['Happy', 'Sad', 'Suprise', 'Angry', 'Disgust', 'Fear', 'Netural'],
        datasets: [
          {
            label: 'Visitor',
            data: this.getOverallBoothData(res.data),
            backgroundColor: [
              'rgba(255, 206, 86, 0.6)',
              'rgba(54, 162, 235, 0.6)',
              'rgba(255, 159, 64, 0.6)',
              ' rgb(255, 154, 162)',
              ' rgb(149, 125, 173)',
              'rgb(170, 169, 173)',
              'rgb(181, 234, 215)',
            ]
          }
        ]
      }

    });
    //console.log(this.state.boothList, 'hello2')
  }



  render() {
   
    return (

      <div className="Background" >
        <Header3 />
        < div class="header" className="ExpoHeader" >
          <div class="backButton" style={{ paddingTop: "1%", paddingRight: "2%" }} >
            <img width="40" height="40" src={require('../../assets/back.png')} onClick={this.onClickBack} />
          </div>
          <div class="expoName" className="ExpoHeaderFont">
            <p> {this.state.boothList.firstName} Booth Infomation</p>
          </div>
        </div>
        {Object.keys(this.state.charOverallData).length && <div className="linechartBlock" >
          {
            <BarChart chartData={this.state.charOverallData} location="bar" legendPosition="bottom" />
          }
          <div style={{ marginLeft: '85%' }} >
            {/* <a >Emotion per hour</a> */}
            <Button variant="info" onClick={this.onClickBarchart} >Emotion per hour</Button>
          </div>
        </div>
        }
        <div className="rowDirect">
          {Object.keys(this.state.chartData).length && <div className="pieBlock" >
            <PieChart chartData={this.state.chartData} location="pie" legendPosition="left" />
          </div>
          }
          <div className="numBlock">
            <div className="chartHeader">
              Total visitor
            </div>
            <div className="chartNumDetail">
              {_.sum(_.values(_.mapValues(this.state.boothList.visitor, 'total')))}
            </div>
          </div>
        </div>
        <div className="rowDirectAvg">
          <div className="doughnutBlockBooth" >
            <div className="chartHeader">
              Emotion Percentage
            </div>
            <div className="emotionPercentList">
              <div className="rowDirect">
                <div className="happyBlock" />
                {"Happy : " + (this.state.visitorData.avgHappy / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="sadBlock" />
                {"Sad : " + (this.state.visitorData.avgSad / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="surpriseBlock" />
                {"Surprise : " + (this.state.visitorData.avgSurprise / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="angryBlock" />
                {"Angry : " + (this.state.visitorData.avgAngry / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="disappointBlock" />
                {"Distgust : " + (this.state.visitorData.avgDisappoint / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="fearBlock" />
                {"Fear : " + (this.state.visitorData.avgFear / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
              <div className="rowDirect">
                <div className="neturalBlock" />
                {"Netural : " + (this.state.visitorData.avgNetural / this.state.visitorData.avgEmotion * 100).toFixed(2) + "%"}
              </div>
            </div>
          </div>
        </div>
      </div >
    );
  }

}
const mapStateToProps = (state) => {
  return {
    data: state.data
  }
}

export default connect(mapStateToProps, null)(BoothOwnchart);