import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import { Form, Card, Button } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import {history} from '../../system/History'
import axios from "axios";
import _ from 'lodash';
import Header2 from '../Nav-bar/nav-bar2'

class editProfile extends Component {
    state = {
        username: '',
        password: '',
        firstname: '',
        lastname: '',
        email: ''
    }
    constructor(props){
        super(props);
        this.state = _.cloneDeep(this.props.data)
    }
    updateUser = async (event) =>{
        event.preventDefault();
        console.log(this.state.username,'ssss')
        if(_.size(this.state.password) < 6){
         alert('The password is to short');
         return;
        }
        try {
            await axios({
                url: `http://localhost:8080/updateuser?userid=${this.props.data.id}`,
                method: 'put',
                data: this.state
            }).then(res => {
                console.log(res.data)
                const { data } = res;
                this.props.adddata(data)
                alert('Update success')
                history.push('/profile');
                history.go()
            }).catch(err => {
                console.warn(err)
                alert('Cannot edit your profile')
                return;
            });
            // await axios({
            //     url:`http://localhost:8080/UserById?id=${this.props.data.id}`,
            //     method: 'get',
            //     headers:{
            //         Authorization: `Bearer ${this.props.data.token}`
            //     }
            // }).then(res => {
            //     const { data } = res;
            //     this.props.adddata(data)
            //     alert('Update success')
            //     history.push('/profile');
            //     history.go()
            // }).catch(e => {
            //     console.warn(e)
            // });
        } catch (error) {
            console.warn(error)
            alert('Cannot edit your profile')
        }
    }
    _handleChange = event => {
        this.setState({[event.target.name]: event.target.value})
    }
    render() {
        const { firstName, lastName, username, email, password } = this.state;
        return (
            <div >
                  <Header2/>
                    <Container>
                        <Form style={{marginLetf:'10%' , marginTop:'5%'}} onSubmit={this.updateUser.bind(this)}>
                            <Form.Label style={{fontSize:'30px'}}>Edit your personal information</Form.Label>
                            <Form.Group >
                                <Form.Label>First name: </Form.Label>
                                <Form.Control name="firstName" type="text" defaultValue={firstName} onChange={this._handleChange.bind(this)} />
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Last name: </Form.Label>
                                <Form.Control name="lastName" type="text" defaultValue={lastName} onChange={this._handleChange.bind(this)}/>
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Username: </Form.Label>
                                <Form.Control name="username" type="text" defaultValue={username} onChange={this._handleChange.bind(this)}/>
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Email address: </Form.Label>
                                <Form.Control name="email" type="email" defaultValue={email} onChange={this._handleChange.bind(this)}/>
                            </Form.Group>
                            <Form.Group >
                                <Form.Label>Password: </Form.Label>
                                <Form.Control name="password" type="password" defaultValue={password} onChange={this._handleChange.bind(this)} />
                            </Form.Group>
                                <Button variant="primary" type="submit" >Save change</Button>
                                
                        </Form>
                    </Container>
            </div>
                  


          

        )
    }
}
const mapStateToProps = (state) => ({
    data: state.data
})
const mapDidpatchToProps = (dispatch) => {
    return {
        adddata: (data) => {
            dispatch({
                type: 'ADD_DATA',
                data: data
            })
        }

    }
}

export default connect(mapStateToProps, mapDidpatchToProps)(editProfile)