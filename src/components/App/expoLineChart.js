import React, { Component } from "react";
import "react-bulma-components/dist/react-bulma-components.min.css";
import "./style.css";
import Chart from "./Chart";
import BarChart from "../Chart/barChart";
import DoughnutChart from "../Chart/doughnutChart";
import PieChart from "../Chart/pieChart";
import LineChart from "../Chart/lineChart";
import { icon } from "react-bulma-components";
import { black } from "ansi-colors";
import axios from "axios";
import { history } from "../../system/History";
import _ from "lodash";
// import { connect } from "react-redux";
//import

class ExpoLineChartPage extends Component {
  constructor() {
    super();
    this.state = {
      chartData: {},
      visitorData: {},
      boothList: []
    };
  }

  componentDidMount() {
    return axios
      .get(`http://localhost:8080/booth`)
      .then(async res => {
        console.log(res.data,'test')
        await this.setState({ boothList: res.data });
        await this.getChartData(res);
        
        //this.getVisitorData(res.data);
      })
      .catch(err => {
        console.log(err);
        alert('Cannor retrive data')
      });
  }

  onClickBack = () => {
   
    history.push("/exposum");
    history.go();
  };

  async getChartData(res) {
    const xLabel = _.chain(this.state.boothList)
      .mapValues(o => o.visitor)
      .map(o => _.map(o, p => p.visitTime))
      .flatten()
      .value();

    let test = [];
    _.forEach(xLabel, val => {
      test = _.unionWith(test, [val], _.isEqual);
    });
    
    const cloneTest = _.cloneDeep(test)


    // Ajax calls here
    const getEmotionValue = type => {
     
      const test = _.map(res.data, val => {
        return _.values(_.mapValues(val.visitor, type));
      });

      console.log(test[0], "www")
     
      const keys = _.keys(test[0]);
      console.log(keys,'keys')
      const test2 = [];
      _.forEach(keys, val => {
        test2.push(_.sum(_.values(_.mapValues(test, val))));
      });
      console.log(test2)
      return test2;
    };
    await this.setState({
      boothList: res.data,
      chartData: {
        labels: _.map(cloneTest,o=>_.join(_.pullAt(o,[0,1,2]),'-')+' '+_.join(o,':')+'0'),
        datasets: [
          {
            label: "Happy",
            data: getEmotionValue("happy"),
            backgroundColor: ["rgba(255, 99, 132, 0.6)"],
            borderColor: "rgba(255, 99, 132, 0.6)",
            fill: false
          },
          {
            label: "Sad",
            data: getEmotionValue("sad"),
            backgroundColor: ["rgba(54, 162, 235, 0.6)"],
            borderColor: "rgba(54, 162, 235, 0.6)",
            fill: false
          },
          {
            label: "Surprise",
            data: getEmotionValue("surprise"),
            backgroundColor: ["rgba(255, 206, 86, 0.6)"],
            borderColor: "rgba(255, 206, 86, 0.6)",
            fill: false
          },
          {
            label: "Angry",
            data: getEmotionValue("angry"),
            backgroundColor: ["rgba(75, 192, 192, 0.6)"],
            borderColor: "rgba(75, 192, 192, 0.6)",
            fill: false
          },
          {
            label: "Disappoint",
            data: getEmotionValue("disgust"),
            backgroundColor: ["rgba(153, 102, 255, 0.6)"],
            borderColor: "rgba(153, 102, 255, 0.6)",
            fill: false
          },
          {
            label: "Fear",
            data: getEmotionValue("fear"),
            backgroundColor: ["rgba(255, 159, 64, 0.6)"],
            borderColor: "rgba(255, 159, 64, 0.6)",
            fill: false
          },
          {
            label: "Netural",
            data: getEmotionValue("netural"),
            backgroundColor: ["rgba(255, 100, 132, 0.6)"],
            borderColor: "rgba(255, 100, 132, 0.6)",
            fill: false
          }
        ]
      }
    });
  }

  render() {
    return (
      <div className="Background">
        {/* hearder */}
        <div class="header" className="ExpoHeader">
          <div
            class="backButton"
            style={{ paddingTop: "1%", paddingRight: "2%" }}
          >
            <img
              width="40"
              height="40"
              src={require("../../assets/back.png")}
              onClick={this.onClickBack}
            />
          </div>
          <div class="expoName" className="ExpoHeaderFont">
            <p> Expo summary</p>
          </div>
        </div>
        {Object.keys(this.state.chartData).length && (
          <div className="linechartBlock">
            <LineChart
              chartData={this.state.chartData}
              location="bar"
              legendPosition="bottom"
            />
          </div>
        )}
      </div>
    );
  }
}

export default ExpoLineChartPage;
