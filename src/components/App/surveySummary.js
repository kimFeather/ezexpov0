import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import BarChartSurvey from '../Chart/barChartSurvey';
import axios from "axios"
import { history } from '../../system/History'
import { connect } from "react-redux";
import Header2 from '../Nav-bar/nav-bar2'
import _ from 'lodash'
import { Col, Container, Row } from 'react-bootstrap'
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

class surveySummary extends Component {
    state = {
        survey: [],
        chartData: {}
    }
    componentDidMount = () => {
        try {
            axios.get(`http://localhost:8080/survey/id?id=${this.props.id}`
            ).then(res => {
                console.log(res)
                this.getChartData(res);

            }).catch(error =>{
                console.error(error);
                alert('Cannot retrieve data')
            })
        } catch (error) {
            console.error(error);
            alert('Cannot retrieve data')
        }
    }
    getChartData(res) {
        // Ajax calls here
        this.getSurveyStat(res.data);
        this.setState({
            survey: res.data
        });
    }

    getSurveyStat(data) {
        const { surveyAnswer } = data;

        const result = this.sumObjectsByKey(...surveyAnswer);
        console.log(result);
        console.log(surveyAnswer)
        const temp = {};
        _.forEach(surveyAnswer, value => {
            _.forEach(value, (val, key) => {
                if (_.includes(key, 'answer')) {
                    temp[key] = _.isEmpty(temp[key]) ? [val] : _.concat(temp[key], [val])
                }
            })
        })
        const myCount = {}
        _.forEach(temp, (val, key) => {
            _.merge(myCount, { [key]: { ..._.countBy(val, Math.floor) } })
        })
        console.log(myCount)
        const tempResult = {};
        _.forEach(myCount, (value, key) => {
            const max =_.max(_.values(value));
            _.forEach([1, 2, 3, 4, 5], val => {
                const count = value[val] ? value[val] : 0
                tempResult[key] = _.isEmpty(tempResult[key]) ? [count] : _.concat(tempResult[key], count)
            });
            this.setState({
                chartData: {
                    ...this.state.chartData,
                    [key]: {
                        labels: ['1(Bad)', '2', '3', '4', '5(exellent)'],
                        datasets: [
                            {
                                label: 'Visitor',
                                data: _.values(tempResult[key]),
                                backgroundColor: [
                                    'rgba(255, 206, 86, 0.6)',
                                    'rgba(54, 162, 235, 0.6)',
                                    'rgba(255, 159, 64, 0.6)',
                                    ' rgb(255, 154, 162)',
                                    ' rgb(149, 125, 173)',
                                  
                                ]
                            }
                        ],
                        max: max
                    }
                }
            })
        })
    }
    sumObjectsByKey(...objs) {
        return objs.reduce((a, b) => {
            for (let k in b) {
                if (b.hasOwnProperty(k) && _.includes(k, 'answer'))
                    a[k] = (a[k] || 0) + b[k];
            }
            return a;
        }, {});
    }

    render() {
        const columns = [{
            dataField: 'sugesstion',
            text: 'List of commend'
        }]
        const { survey, chartData } = this.state;

        let surveyAnswer = []

        if (this.state.survey.surveyAnswer)
            surveyAnswer = _.filter(this.state.survey.surveyAnswer, value => {
                return !_.isEmpty(value.sugesstion)
            })
        console.log(surveyAnswer);
        
        return (

            <div>
                <Header2 />
                <Container>
                    <Row>
                        <Col>
                            <p style={{ fontSize: '20px', marginTop: '1em', fontWeight: 'bold' }}>{survey.question1}</p>
                            {!_.isEmpty(chartData['answer1']) && <div className="linechartSurvey" >
                                {
                                    <BarChartSurvey chartData={chartData['answer1']} location="bar" legendPosition="bottom" />
                                }
                            </div>
                            }</Col>
                        <Col>
                            <p style={{ fontSize: '20px', marginTop: '1em', fontWeight: 'bold' }}>{survey.question2}</p>
                            {!_.isEmpty(chartData['answer2']) && <div className="linechartSurvey" >
                                {
                                    <BarChartSurvey chartData={chartData['answer2']} location="bar" legendPosition="bottom" />
                                }
                            </div>
                            }</Col>
                    </Row>
                    <Row>
                        <Col>
                            <p style={{ fontSize: '20px', marginTop: '1em', fontWeight: 'bold' }}>{survey.question3}</p>
                            {!_.isEmpty(chartData['answer3']) && <div className="linechartSurvey" >
                                {
                                    <BarChartSurvey chartData={chartData['answer3']} location="bar" legendPosition="bottom" />
                                }
                            </div>
                            }</Col>
                        <Col>
                            <p style={{ fontSize: '20px', marginTop: '1em', fontWeight: 'bold' }}>{survey.question4}</p>
                            {!_.isEmpty(chartData['answer4']) && <div className="linechartSurvey" >
                                {
                                    <BarChartSurvey chartData={chartData['answer4']} location="bar" legendPosition="bottom" />
                                }
                            </div>
                            }</Col>

                    </Row>
                    <Row>
                        <Col md={{ span: 6, offset: 3 }}>
                            <p style={{ fontSize: '20px', marginTop: '1em', fontWeight: 'bold' }}>{survey.question5}</p>
                            {!_.isEmpty(chartData['answer5']) && <div className="linechartSurvey" >
                                {
                                    <BarChartSurvey chartData={chartData['answer5']} location="bar" legendPosition="bottom" />
                                }
                            </div>
                            }
                        </Col>
                    </Row>
                    {

                        <BootstrapTable keyField='sugesstion' data={surveyAnswer.map(sur =>  sur)} columns={columns} />
                    }





                </Container>
            </div>


        )
    }
}
const mapStateToProps = (state) => {
    return {
        id: state.boothId
    }
}
export default connect(mapStateToProps, null)(surveySummary)