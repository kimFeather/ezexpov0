import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import { Router, Route , Link ,  } from 'react-router';
import { icon } from 'react-bulma-components';
import { black } from 'ansi-colors';
//import 

class ExpoPage extends Component {
  state = {
    expos: ['expo1', 'expo2', 'expo3']
  }
  

  onClickExpo = () => {
    const { push } = this.props
    push('/chart')
}

  render() {
    return (
      <div className="Background">

        {/* hearder */}
        <div class="header" className="HeaderBar">
          <p class="control has-icons-left has-icons-right">
            <div class="control" className="inputBox">
              <input class="input" type="search" placeholder="search" />
              <span class="icon is-small is-left">
                <i class="fas fa-envelope"></i>
              </span>

            </div>
          </p>

        </div>
        {/* mid */}
        <div class="flex-container" >
          
          <div class="content" className="RowContent" >
           

            <div className="Block" onClick={this.onClickExpo} >
            {/* <p><Link to={"/booth"}>1</Link>
              </p> */}
              <p>
              </p>
            </div>

            <div className="Block">
              <p>2</p>
            </div>

            <div className="Block">
              <p>3</p>
            </div>

            <div className="Block">
              <p>4</p>
            </div>

          </div>

          <div class="content" className="RowContent" >
           

            <div className="Block">
              <p>1</p>
            </div>

            <div className="Block">
              <p>2</p>
            </div>

            <div className="Block">
              <p>3</p>
            </div>

            <div className="Block">
              <p>4</p>
            </div>

          </div>

          <div class="content" className="RowContent" >
           

            <div className="Block">
              <p>1</p>
            </div>

            <div className="Block">
              <p>2</p>
            </div>

            <div className="Block">
              <p>3</p>
            </div>

            <div className="Block">
              <p>4</p>
            </div>

          </div>

          <div class="content" className="RowContent" >
           

            <div className="Block">
              <p>1</p>
            </div>

            <div className="Block">
              <p>2</p>
            </div>

            <div className="Block">
              <p>3</p>
            </div>

            <div className="Block">
              <p>4</p>
            </div>

          </div>

        </div>
        {/* footer */}
        <div>

        </div>
        {/* <ul>
          {this.state.booths.map(booth => <li key={booth.id}>{booth}</li>)}
        </ul> */}

      </div>
    )
  }
}

export default ExpoPage;
