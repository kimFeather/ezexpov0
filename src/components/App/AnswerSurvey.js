import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { Button, Form, Card,Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import FormCheck from 'react-bootstrap/FormCheck'
import { history } from '../../system/History'
import axios from "axios";
import Header2 from '../Nav-bar/nav-bar2'
import { connect } from 'react-redux';
import _ from 'lodash';

const questionRadios = ['radio', 'radio', 'radio', 'radio', 'radio']

class SurveyAnswer extends Component {
    state = {
        survey: [],
        answers:{
            sugesstion: ''
        }
    }
    componentDidMount = () => {
        console.log(this.props.id)
        try {
            axios({
                url: `http://localhost:8080/survey/id?id=${this.props.id}`,
                method: 'get'
            }).then(res => {
                this.setState({
                    survey: res.data
                })
                console.log('survey', this.state.survey)
                console.log('survey', this.state.survey.surveyName)
            });
        } catch (error) {
            console.error(error);
            alert('Cannot retrieve data')
        }
    }
    handleChange(event){
        const answer = _.split(event.target.value, '-');
        this.setState({
            answers: {
                ...this.state.answers,
                [answer[0]]: _.parseInt(answer[1])
            }
        })
    }
    handleSuggestionChange(event){
        this.setState({
            answers:{
                ...this.state.answers,
                sugesstion: event.target.value
            }
        })
    }
    handleSubmit(event){
        event.preventDefault();
        console.log(this.state.answers)
        if(this.state.answers.question1 == null || this.state.answers.question2 == null || this.state.answers.question3 == null || this.state.answers.question4 == null || this.state.answers.question5 ==  null){
            alert('Please answer all question')
            return;
        }
        try {
            axios({
                url: `http://localhost:8080/answerSurvey?id=${this.props.id}`,
                method: 'post',
                data: {
                   answer1:this.state.answers.question1,
                   answer2:this.state.answers.question2,
                   answer3:this.state.answers.question3,
                   answer4:this.state.answers.question4,
                   answer5:this.state.answers.question5,
                   sugesstion:this.state.answers.sugesstion

                }
            }).then(() => {
                alert('Thank you for your answer')
                window.location.reload();
            }).catch(err => {
                console.warn(err)
                alert('Cannot submit your answer')
                return;
            });
        } catch (error) {
            console.error(error)
        }
    }
    render() {
        return (
            <div >
               <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>
                <Card border="secondary" style={{ marginLeft: '33em', marginTop: '12em', height: '615px' ,width:'900px'}} >
                    <Container >
                        <Form style={{ margin: '2em' }} onSubmit={this.handleSubmit.bind(this)}>
                            <p style={{ fontSize: '30px', fontWeight: 'bold' }}>Please answer the survey</p>
                            {_.map(questionRadios, (value, key) => {
                                return (
                                    <Form.Group controlId='form1' >
                                        <Form.Label style={{ fontSize: '20px' }}>{this.state.survey[`question${key+1}`]} : </Form.Label>
                                        <div style={{ marginLeft: '10px' }}>
                                            {_.map([1, 2, 3, 4, 5], val => {
                                                return (<Form.Check
                                                    inline
                                                    type={`${value}`}
                                                    label={_.isEqual(val, 1) ? `${val} (Bad)` : _.isEqual(val, 5) ? `${val} (Execelent)` : `${val}`}
                                                    // label={`${val}`}
                                                    name={`question-${key}`}
                                                    id={`question${key}-${val}`}
                                                    value={`question${key+1}-${val}`}
                                                    onChange={this.handleChange.bind(this)}
                                                />)
                                            })}
                                        </div>
                                    </Form.Group>
                                )
                            })}
                            <Form.Group  >
                                <Form.Label style={{ fontSize: '20px' }}>Any sugesstion (optional) ? : </Form.Label>
                                <Form.Control name="advice" type="text" style={{ width: '500px' }} value={this.state.answers.sugesstion} onChange={this.handleSuggestionChange.bind(this)}/>
                            </Form.Group>
                            <Button variant="primary" type="submit" >Submit your answer</Button>
                        </Form>
                    </Container>
                </Card>



            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        id: state.boothId
    }
}

export default connect(mapStateToProps, null)(SurveyAnswer) 