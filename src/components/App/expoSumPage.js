import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import Chart from './Chart';
import BarChart from '../Chart/barChart';
import DoughnutChart from '../Chart/doughnutChart';
import PieChart from '../Chart/pieChart';
import { icon } from 'react-bulma-components';
import { black } from 'ansi-colors';
import axios from "axios"
import { history } from '../../system/History'
import Header2 from '../Nav-bar/nav-bar2'
import { Button } from 'react-bootstrap'
import _ from 'lodash'

class ExpoSumPage extends Component {

    constructor() {
        super();
        this.state = {
            chartData: {},
            visitorData: {},
            boothList: [],
            chartOverallData: {},
        }
    }

    componentDidMount() {

        axios
            .get(`http://localhost:8080/booth`)
            .then(res => {
                console.log(res);
                console.log("in axios");
                this.getChartData(res);
                this.getOverAllChartData(res)
                this.getVisitorData(res.data);

            }).catch(error => {
                console.log(error)
                alert('Cannot retrieve data')
            })
    }




    onClickBack = () => {
        console.log("click back from boothpage" + "booth id")
        console.log(this.props)
        history.push('/booth');
        history.go()
    }

    onClickBarchart = () => {
        console.log("click go linechart from boothpage" + "booth id")
        console.log(this.props)
        history.push('/expolinechart');
        history.go()
    }

    getBoothData(boothList) {
        //happy
        const booths = boothList.map((booth, index) => {
            //happy
            const avg = {}
            Object.keys(booth['visitor'][0])
                .filter(key => key !== 'visitTime' && key !== 'total')
                .map(key => avg[key] = booth['visitor'].reduce((sum, visitor) => {
                    console.log(key, "key")
                    return sum + parseFloat(visitor[key])
                }, 0) / booth['visitor'].length)
            return avg
          

        }
        )

        console.log(booths, 'booths')


        const avgBooths = Object.keys(booths[0]).map(key => booths.reduce((sum, obj) => {
            return sum + parseFloat(obj[key])
        }, 0) / booths.length
        )

        console.log(avgBooths)

        return avgBooths



    }
    getOverAllBoothData(boothList) {
        //happy
        const booths = boothList.map((booth, index) => {
            //happy
            const overall = {}
            Object.keys(booth['visitor'][0])
                .filter(key => key !== 'numOfMale' && key !== 'numOfFemale' && key !== 'visitTime')
                .map(key => overall[key] = booth['visitor'].reduce((sum, visitor) => {
                    return sum + parseFloat(visitor[key])
                }, 0))

            return overall

        }
        )

        console.log(booths, 'booths')
        const overAllBooths = Object.keys(booths[0]).map(key => booths.reduce((sum, obj) => {
            return sum + parseFloat(obj[key])
        }, 0)
        )

        console.log(overAllBooths)

        return overAllBooths

    }

    getVisitorData(boothList) {
        const visitors = boothList.map((booth, index) => {

            //happy
            var avgHappy = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.happy);
            }, 0);
            console.log("avg avgHappy" + avgHappy)

            //sad
            var avgSad = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.sad);
            }, 0);
            console.log("avg avgSad" + avgSad)

            //surprise
            var avgSurprise = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.surprise);
            }, 0);
            console.log("avg avgSurprise" + avgSurprise)

            //angry
            var avgAngry = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.angry);
            }, 0);
            console.log("avg avgAngry" + avgAngry)

            //disappoint
            var avgDisappoint = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.disgust);
            }, 0);
            console.log("avg avgDisgust" + avgDisappoint)

            //fear
            var avgFear = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.fear);
            }, 0);
            console.log("avg avgFear" + avgFear)

            //netural
            var avgNetural = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.netural);
            }, 0);
            console.log("avg avgNetural" + avgNetural)

            //av==emotion
            var avgEmotion = booth['visitor'].reduce((sum, visitor) => {
                return sum
                    + parseFloat(visitor.happy)
                    + parseFloat(visitor.sad)
                    + parseFloat(visitor.surprise)
                    + parseFloat(visitor.angry)
                    + parseFloat(visitor.disgust)
                    + parseFloat(visitor.fear)
                    + parseFloat(visitor.netural);
            }, 0);
            console.log("avg emotion" + avgEmotion)

            //men visitor
            var avgMenVisitor = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.numOfMale);
            }, 0);

            //women visitor
            var avgWomenVisitor = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.numOfFemale);
            }, 0);

            //avg total
            var avgVisitor = booth['visitor'].reduce((sum, visitor) => {
                return sum + parseFloat(visitor.total);
            }, 0);

            console.log("avg avgVisitor" + avgVisitor)


            return {
                avgMenVisitor,
                avgWomenVisitor,
                avgVisitor,
                // avgHappy

            }


        })

        const totalMen = visitors.reduce((sum, visitor) => {
            return sum + parseFloat(visitor.avgMenVisitor);
        }, 0)

        const totalWomen = visitors.reduce((sum, visitor) => {
            return sum + parseFloat(visitor.avgWomenVisitor);
        }, 0)

        const totalVisitors = visitors.reduce((sum, visitor) => {
            return sum + parseFloat(visitor.avgVisitor);
        }, 0)

        // const happyPercent = visitors.reduce((sum, visitor) => {
        //     return sum + parseFloat(visitor.avgHappy);
        // }, 0)


        this.setState({
            visitorData: {
                totalMen,
                totalWomen,
                totalVisitors,
                // happyPercent
            }
        })
    }


    getChartData(res) {
        // Ajax calls here
        this.setState({
            boothList: res.data,
            chartData: {
                labels: ['Happy', 'Sad', 'Suprise', 'Angry', 'Disgust', 'Fear', 'Netural'],
                datasets: [
                    {
                        label: 'Visitor',
                        data: this.getBoothData(res.data),
                        backgroundColor: [
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            ' rgb(255, 154, 162)',
                            ' rgb(149, 125, 173)',
                            'rgb(170, 169, 173)',
                            'rgb(181, 234, 215)'
                        ]
                    }
                ]
            }
        });

    }
    getOverAllChartData(res) {
        this.setState({
            boothList: res.data,
            chartOverallData: {
                labels: ['Happy', 'Sad', 'Suprise', 'Angry', 'Disgust', 'Fear', 'Netural'],
                datasets: [
                    {
                        label: 'Visitor',
                        data: this.getOverAllBoothData(res.data),
                        backgroundColor: [
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            ' rgb(255, 154, 162)',
                            ' rgb(149, 125, 173)',
                            'rgb(170, 169, 173)',
                            'rgb(181, 234, 215)',
                        ]
                    }
                ]
            }

        });
        console.log(this.state.boothList, 'hello2')
    }

    render() {
        return (

            <div className="Background" >
                <Header2 />
                {/* hearder */}
                < div class="header" className="ExpoHeader" >

                    <div class="backButton" style={{ paddingTop: "1%", paddingRight: "2%" }} >
                        <img width="40" height="40" src={require('../../assets/back.png')} onClick={this.onClickBack} />
                    </div>
                    <div class="expoName" className="ExpoHeaderFont">

                        <p> Expo summary</p>

                    </div>
                </div>

                {Object.keys(this.state.chartOverallData).length && <div className="linechartBlock" >
                    <BarChart chartData={this.state.chartOverallData} location="bar" legendPosition="bottom" />
                    <div style={{ marginLeft: '85%' }} >
                        {/* <a >Emotion per hour</a> */}
                        <Button variant="info" onClick={this.onClickBarchart} >Emotion per hour</Button>
                    </div>

                </div>
                }

                <div className="rowDirect">


                    {Object.keys(this.state.chartData).length && <div className="pieBlock" >
                        <PieChart chartData={this.state.chartData} location="pie" legendPosition="left" />
                    </div>
                    }

                    <div className="numBlock">
                        <div className="chartHeader">
                            Total visitor
                </div>
                        <div className="chartNumDetail">
                            {this.state.visitorData.totalVisitors}

                        </div>
                    </div>
                </div>

                <div className="rowDirect">


                    {Object.keys(this.state.chartOverallData).length && <div className="doughnutBlock" >

                        {console.log((this.state.chartOverallData.datasets[0].data[0] / this.state.visitorData.totalVisitors * 100).toFixed(2) + ": this is overall happy")}
                        <div className="chartHeader">
                            Emotion Percentage
                        </div>
                        <div className="emotionPercentList">
                            <div className="rowDirect">
                                <div className="happyBlock" />
                                {"Happy : " + (this.state.chartOverallData.datasets[0].data[0] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="sadBlock" />
                                {"Sad : " + (this.state.chartOverallData.datasets[0].data[1] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="surpriseBlock" />
                                {"Surprise : " + (this.state.chartOverallData.datasets[0].data[2] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="angryBlock" />
                                {"Angry : " + (this.state.chartOverallData.datasets[0].data[3] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="disappointBlock" />
                                {"Disappoint : " + (this.state.chartOverallData.datasets[0].data[4] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="fearBlock" />
                                {"Fear : " + (this.state.chartOverallData.datasets[0].data[5] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>
                            <div className="rowDirect">
                                <div className="neturalBlock" />
                                {"Netural : " + (this.state.chartOverallData.datasets[0].data[6] / this.state.visitorData.totalVisitors * 100).toFixed(2) + "%"}
                            </div>


                        </div>
                    </div>
                    }


                </div>

            </div >
        );
    }

}

export default ExpoSumPage;