import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import 'typeface-roboto';
import axios from "axios"
import { Form, Card, Button, Navbar, Nav } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'

class Login extends Component {
    state = {
        username: '',
        password: '',
        email: ''
    }
    login = (event) => {
        event.preventDefault();
        axios({
            url: `http://localhost:8080/auth`,
            method: 'post',
            data: {
                username: this.state.username,
                password: this.state.password
            }

        }).then(res => {
            const { data } = res
            const { user } = data
            console.log(data)

            if (data.userRole == "BOOTHOWNER") {
                this.props.adddata(data)
                console.log(this.props)
                //alert('sucess')
                history.push('/boothOwnChart');
                history.go()
            }
            if (data.userRole == "EVENTORGANIZER") {
                this.props.adddata(data)
                console.log(this.props)
                //alert('sucess')
                history.push('/booth');
                history.go()

            }




        }).catch(e => {
            console.log(e.message)
            if (e.message === "Request failed with status code 401") {
                alert("Username and password does not match")
            } else {
                alert("Cannot retreive data ")
            }
            console.log("error " + e)
            alert("Cannot retreive data")
        })



    }
    onUsernameChange = event => {
        const username = event.target.value;
        this.setState({ username });
        console.log(this.state.username)
    };
    onPasswordChange = event => {
        const password = event.target.value;
        this.setState({ password });
        console.log(this.state.password)
    };
    forgetPassword = () => {
        history.push('/forgetPassword')
        history.go()
    }
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>

                <Container>


                    <Form style={{ margin: '10em' }} onSubmit={this.login.bind(this)} >
                        <Form.Label style={{ fontSize: '30px', textalign: 'center' }}>Login</Form.Label>
                        <Form.Group >
                            <Form.Label>Username</Form.Label>
                            <Form.Control onChange={this.onUsernameChange} type="text" placeholder="Enter username" required />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control onChange={this.onPasswordChange} type="password" placeholder="Password" required />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Login
                        </Button>
                        <Button variant="link" onClick={this.forgetPassword.bind(this)}>
                            Forget password
                        </Button>
                    </Form>
                </Container>
            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    username: state.username,
    password: state.password,
    email: state.email,
    data: state.data
})

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        },
        adddata: (data) => {
            dispatch({
                type: 'ADD_DATA',
                data: data
            })
        }

    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)
    (Login)