import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import './style.css';
import LineChart from '../Chart/lineChart';


class Linechart extends Component {
    state = {
        chartData: {}
    }

    componentDidMount() {
        console.log(this.props.boothData['visitor'])
        const { boothData } = this.props
        const booths = Object.keys(boothData['visitor'][0])
        .filter(key => key !== 'numOfMale' && key !== 'numOfFemale' && key !== 'visitTime')
        .map(key => boothData['visitor'].map(booth => booth[key]))
        console.log(booths)
    }

    render() {
        return (
            <div>
                <LineChart chartData={this.state.chartData} location="bar" legendPosition="bottom" />
            </div>
        )
    }
}

export default Linechart