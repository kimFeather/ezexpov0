import React, { Component } from 'react';
import { Router, Route } from 'react-router';
import { connect } from 'react-redux'
import 'typeface-roboto';
import axios from "axios"
import { Form, Card, Button, Navbar } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import _ from 'lodash';

class ChangePassword extends Component {
    state = {
        newPassword: '',
        confirmPassword: ''
    }
    onNewPasswordChange = event => {
        const newPassword = event.target.value;
        this.setState({ newPassword });
        console.log(this.state.newPassword)
    };
    onConfirmPasswordChange = event => {
        const confirmPassword = event.target.value;
        this.setState({ confirmPassword });
        console.log(this.state.confirmPassword)
    };
    changePassword = async (event) => {
        event.preventDefault();
        if(_.size(this.state.newPassword) < 6){
            alert('The password is to short');
            return;
         }
        if (this.state.newPassword == this.state.confirmPassword) {
            console.log('enterChangePassword')
            await axios({
                url: `http://localhost:8080/updateuser?userid=${this.props.data.id}`,
                method: 'put',
                data: {
                    password: this.state.newPassword
                }
            }).catch(err => {
                console.warn(err)
                alert('Cannot change your password')
                return;
            });
            await axios({
                url: `http://localhost:8080/UserById?id=${this.props.data.id}`,
                method: 'get',

            }).then(res => {
                const { data } = res
                console.log(data)
                this.props.adddata(data)
                console.log(this.props)
                alert("Password change completed")
                history.push('/login');
                history.go()
            }).catch(e => {
                console.log("error " + e)
                alert("Cannot change your password")
                return;
            })


        } else {
            alert('Password does not matched')
        }
    }
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand >EzExpo</Navbar.Brand>
                </Navbar>
                <Container>
                    <Form style={{ margin: '20em' }} onSubmit={this.changePassword.bind(this)}>
                        <h3 style={{ fontStyle: 'bold' }}>User: {this.props.data.username}</h3>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control onChange={this.onNewPasswordChange} type="password" placeholder="New password" required />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control onChange={this.onConfirmPasswordChange} type="password" placeholder="Confirm new password" required />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                  </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    data: state.data
})
const mapDidpatchToProps = (dispatch) => {
    return {
        adddata: (data) => {
            dispatch({
                type: 'ADD_DATA',
                data: data
            })
        }

    }
}

export default connect(mapStateToProps, mapDidpatchToProps)(ChangePassword)