import React, { Component } from 'react';
import { Button, Navbar } from 'react-bootstrap'
import { history } from '../../system/History'

export default class HomePage extends Component {
    
    goRegister = ()=>{
        history.push('/VisitorRegisterForm')
        history.go()
    }

    goLogin =()=>{
        history.push('/login')
        history.go()
    }

    render() {
        return (
            <div style={{ flex: '1' }}>
                <div style={{ backgroundColor: 'rgb(15, 21, 33)', width: '120em', height: '50em' }}>
                    {/* < > */}
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <h1 style={{ textAlign: 'center', color: 'white' }} >Welcome to EzExpo</h1>
                    <p style={{ textAlign: 'center', color: 'white' }}>Event mangement web application with facial expression technology</p>
                    <div style={{flexDirection:'row',marginLeft:'45%'}}>
                        <Button variant="warning" onClick={this.goRegister.bind(this)} >Pre-register our exhibition</Button>
                    </div>
                    <br /><br /><br /><br /><br /><br /><br /><br />
                    <div style={{marginLeft:'80%'}}>
                        <p style={{ marginLeft:'30px', color: 'white' }}>For Event organizer and booth owner</p>
                        <Button variant="warning"  onClick={this.goLogin.bind(this)}>Login as event organizer or booth owner</Button>
                    </div>
                </div>
              


            </div>

        )
    }
}