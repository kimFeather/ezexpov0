import React, { Component } from 'react';
import { Button } from 'react-bootstrap'
import axios from "axios";
import _ from 'lodash';
import Header2 from '../Nav-bar/nav-bar2'
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends Component {
    state = {
        visitorList: []
    }

    componentWillMount() {
        try {
            axios({
                url: `http://localhost:8080/visitor`,
                method: 'get'
            }).then(res => {
                const test = _.map(res.data, (val, key) => {
                    return {
                        ...val,
                        id: key+1
                    }
                })  
                this.setState({
                    visitorList: test
                })
                console.log('userlist', this.state.visitorList)
            })
                .catch(error => {
                    console.log(error)
                    alert('Cannot retrieve data')
                })
        } catch (error) {
            console.error(error);
            alert('Cannot retrieve data')
        }

    }

   

    render() {
       

        const columns = [
        {
            dataField: 'id',
            text: 'Id',
            headerStyle: { backgroundColor: 'gray' ,width:'5em',textAlign: 'center'}
        },
        {
            dataField: 'name',
            text: 'Name',
            headerStyle: { backgroundColor: 'gray' ,width:'10em',textAlign: 'center'},


        }, {
            dataField: 'email',
            text: 'Email',
            headerStyle: { backgroundColor: 'gray' ,width:'15em',textAlign: 'center'}
        },
        {
            dataField: 'phoneNumber',
            text: 'Phone number',
            headerStyle: { backgroundColor: 'gray',width:'8em',textAlign: 'center' }
        },
        {
            text: 'Sign',
            headerStyle: { backgroundColor: 'gray' ,width:'15em' ,textAlign: 'center'}
        },

        ];
        return (
            <div>
                <div style={{ margin: 'auto', marginTop:'50px' }}>
                    <h3 style={{ textAlign: 'center', fontWeight: 'bold' }}>EzExpo visitor registration form</h3>
                    {/* <h3 style={{ textAlign: 'center', fontWeight: 'bold' }}>Date: 27/11/2019</h3> */}
                </div>
                <div style={{ width: '90%', margin: 'auto', marginTop: '100px' }}>
                    <BootstrapTable keyField='id' data={this.state.visitorList} columns={columns} ></BootstrapTable>
                </div>
                
            </div>

        )
    }
}
class VisitorRegistration extends Component {
    render() {
      return (
        <div>
          <ReactToPrint
            trigger={() => <Button variant="primary" style={{marginTop:'50px',marginLeft:'85%'}}>Print enrollment sheet</Button>}
            content={() => this.componentRef}
            // pageStyle='@page { size: auto; margin: 0mm; } @media print { body { -webkit-print-color-adjust: exact; padding: 40px !important; } }'
          />
          <ComponentToPrint ref={el => (this.componentRef = el)} />
        </div>
      );
    }
  }
  
  

export default VisitorRegistration