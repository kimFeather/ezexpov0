import React, { Component } from 'react';
import { Button, Form } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import { history } from '../../system/History'
import axios from "axios";
import Header2 from '../Nav-bar/nav-bar2'
import LineChart from '../Chart/linChartVisitorRegister'
import _ from 'lodash'
import { connect } from 'react-redux'

class Announcement extends Component {
    state = {
        announcement: [],
        editAnnouncement: '',
        visitorList: [],
        chartData: {},
    }


  

    componentDidUpdate() {
       
        console.log('visitorlist2',this.state.visitorList)
        // const test = _.filter(this.state.visitorList,o=>o.registerTime[1]===11&&o.registerTime[2]===8&&o.registerTime[3]==15&&o.registerTime[4]==2)
        const test = _.filter(this.state.visitorList,o=>_.isEqual(o.registerTime, [2019,11,8,15,20]))
        console.log(_.size(test))
    }


    updateAnnouncement = (event) => {
        event.preventDefault();
        try {
            axios({
                url: `http://localhost:8080/updateAnnouncement?userid=${this.props.data.id}`,
                method: 'put',
                data: {
                    announcement: this.state.editAnnouncement
                }
            }).then(() => {
                alert('Update complete')
                history.push('/booth');
                history.go()
            }).catch(err => {
                console.warn(err)
                alert('Cannot update')
                return;
            });
        } catch (error) {
            console.error(error)
        }
    }

    _handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }

    componentDidMount = () => {
        try {
            axios({
                url: `http://localhost:8080/announcement/id?id=${this.props.data.id}`,
                method: 'get'
            }).then(res => {
                this.setState({
                    announcement: res.data
                })
                console.log('announcement', this.state.announcement)

            }).catch(err => {
                console.warn(err)
                alert('Cannot retreive data')
                return;
            });
        } catch (error) {
            console.error(error)
        }
        try {
            axios({
                url: `http://localhost:8080/visitor`,
                method: 'get'
            }).then(res => {
                this.setState({
                    visitorList: res.data
                })
                this.getChartData(res);
                console.log('visitorlist', this.state.visitorList)
            })
                .catch(error => {
                    console.log(error)
                    alert('Cannot retrieve data')
                })
        } catch (error) {
            console.error(error);
            alert('Cannot retrieve data')
        }
        
    }

    getChartData(res) {
        // Ajax calls here
        const xLabel =_.mapValues(this.state.visitorList, o=>o.registerTime)
        console.log(xLabel)
        let test = []
        _.forEach(xLabel, val => {
            test = _.unionWith(test, [val], _.isEqual)
        })
        console.log(test);
        console.log(_.map(test,o=>_.join(o,'-')))
        const getNumVisitor = (x)=>{
            
            const test = _.filter(this.state.visitorList,o=>_.isEqual(o.registerTime, x))
            return _.size(test)
        }
        const cloneTest = _.cloneDeep(test)
        this.setState({
            visitorList: res.data,
            chartData: {
                labels: _.map(cloneTest,o=>_.join(_.pullAt(o,[0,1,2]),'-')+' '+_.join(o,':')),
                datasets: [
                    {
                        label: 'Visitor',
                        data: _.map(test,o=>getNumVisitor(o)),
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.6)'
                        ],
                        borderColor: 'rgb(255, 99, 132)',
                        fill: false,
                        lineTension:0

                    }

                ]
            }
        });
        //console.log(this.state.boothList, 'hello')

    }



    render() {
        return (
            <div>
                <Header2 />
                <Container>
                    <Form style={{ marginLeft: '10%', marginTop: '2%' }} onSubmit={this.updateAnnouncement.bind(this)} >
                        <p style={{ fontSize: '40px', fontWeight: "bold", marginBottom: '2%' }}>Edit exhibition announcement</p>
                        <Form.Group >
                            <Form.Label>Announcement: </Form.Label>
                            <Form.Control name="editAnnouncement" type="text" defaultValue={this.state.announcement.announcement} onChange={this._handleChange.bind(this)} required />
                        </Form.Group>
                        <Button variant="primary" type="submit" >Change</Button>
                    </Form>
                </Container>
                <h3 style={{ textAlign: 'center', margin: '3em' }}>{this.state.visitorList.length}: Amount of participant</h3>

                {Object.keys(this.state.chartData).length && <div style={{width:'75%',height:'70%',marginLeft:'15em'}} >
                    <LineChart chartData={this.state.chartData} location="bar" legendPosition="bottom" />
                    
                </div>
                }
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    data: state.data
})
export default connect(mapStateToProps, null)(Announcement) 