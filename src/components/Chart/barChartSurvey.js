import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

class BarChartSurvey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartData: props.chartData
        }
        console.log(this.state.chartData);
        
    }

    static defaultProps = {
        displayTitle: true,
        displayLegend: true,
        legendPosition: 'right',
        location: 'City'
    }

    render() {
        const {chartData} = this.state;
        const {max} = chartData
        return (
            <div className="chart">
                <Bar
                    data={chartData}
                    options={{
                        title: {
                            display: this.props.displayTitle,
                            text: 'Number of answer of each rating',
                            fontSize: 25,
                            fontColor: "#282c34"
                        },
                        legend: {
                            display: this.props.displayLegend,
                            position: this.props.legendPosition
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                    min: 0,
                                    max: max + 1,
                                    stepSize: 1  
                                }
                              }]
                           }
                    }}
                />

            </div>
        )
    }
}

export default BarChartSurvey;