import React, {Component} from 'react';
import {Line} from 'react-chartjs-2';
import { black } from 'ansi-colors';

class LineChartVisitorRegister extends Component{
    constructor(props){
      super(props);
      this.state = {
        chartData:props.chartData
      }
    }
  
    static defaultProps = {
     
      displayTitle:true,
      displayLegend: true,
      legendPosition:'right',
      location:'City'
    }
   
  
    render(){
      return (
        <div className="chart">
          <Line
          
            data={this.state.chartData}
            options={{
              title:{
                display:this.props.displayTitle,
                text:'Number of application per hour ',
                fontSize:25,
                fontColor: "#282c34"
              },
              legend:{
                display:this.props.displayLegend,
                position:this.props.legendPosition
              },
              scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        min: 0,
                        stepSize: 1  
                    }
                  }]
               }
            }}
          />
  
        </div>
      )
    }
  }
  
  export default LineChartVisitorRegister;